var currentPage = 1;
var totalAmount = 0;
var temptotal;
function next(){
    if(currentPage!=7){
        totalAmount = parseInt($("#totalValue").html());
        $(".section-content").hide();
        notes();
        currentPage = currentPage + 1;
        $("#" + currentPage).show();
        $(".number").html(currentPage);
        $(".text").html(currentPage);
    }   
}

function prev(){
    $(".section-content").hide();
    currentPage = currentPage - 1;
    $("#" + currentPage).show();
    $(".number").html(currentPage);
    $(".text").html(currentPage);

}

function start(){
    location.reload();
}

function notes(){
    if(currentPage == 2)
    $('#table').append('<tr class="particulars"><td class="comment">Total Application Pages - ' + $("#pageCount").val() + '</td><td class="price">'+ totalAmount +'</td></tr>');
    else if(currentPage == 3)
    $('#table').append('<tr class="particulars"><td class="comment">Price variation due to technology </td><td class="price">'+ totalAmount +'</td></tr>');
    else if(currentPage == 4)
    $('#table').append('<tr class="particulars"><td class="comment">QA charges added </td><td class="price">'+ totalAmount +'</td></tr>');
    else if(currentPage == 5)
    $('#table').append('<tr class="particulars"><td class="comment">Deployment charges added </td><td class="price">'+ totalAmount +'</td></tr>');
    else if(currentPage == 6)
    $('#table').append('<tr class="particulars"><td class="comment">Documentation charges added </td><td class="price">'+ totalAmount +'</td></tr>');
}

function calculate(){
    // console.log("Changed - New Value" + $("#pageCount").val());
    totalAmount = $("#pageCount").val() * 15000;
    $("#totalValue").html(totalAmount);

}
function technologies(){
    totalAmount= $("#dropdown").val() *  $("#pageCount").val();
    $("#totalValue").html(totalAmount);
}
function percentage(){
    temptotal=( totalAmount * $('input[name="testing"]:checked').val()) /100;
    $("#totalValue").html(parseInt(temptotal)+ parseInt(totalAmount));
}

function hosting(){
    temptotal =  parseInt(totalAmount) + parseInt($('input[name="hosting"]:checked').val());
    $("#totalValue").html(temptotal);
}

function documentation(){
    temptotal =  parseInt(totalAmount) + parseInt($('input[name="documentation"]:checked').val());
    $("#totalValue").html(temptotal);
}